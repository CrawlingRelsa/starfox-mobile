﻿

using UnityEngine;

public abstract class AbstractObstacle : MonoBehaviour
{
    [Header("Explosion")]
    public ParticleSystem explosion;

    [Header("Base")]
    [SerializeField]
    private int life = 100;

    [SerializeField]
    private float speed = 1.0f;

    [SerializeField]
    private int damageOnHit = 1;

    [SerializeField]
    private Vector3 direction;

    [SerializeField]
    private bool isDestroyable = true;

    private GameObject player;
    protected GameObject mainCamera;

    public int Life { get => life; set => life = value; }
    public float Speed { get => speed; set => speed = value; }
    public int DamageOnHit { get => damageOnHit; set => damageOnHit = value; }
    public Vector3 Direction { get => direction; set => direction = value; }
    public bool IsDestroyable { get => isDestroyable; set => isDestroyable = value; }
    public GameObject Player { get => player; set => player = value; }


    public abstract void Fire();
    public abstract void Move();

    private void Awake()
    {
        Player = GameObject.FindWithTag("Player");
        mainCamera = GameObject.FindWithTag("MainCamera");
    }

    private void Start()
    {
        transform.LookAt(Player.transform);
    }

    private void Update()
    {
        if (!GameState.GetInstance().IsPaused)
        {
            //DestroyOnOutOfScreen();
            ApplyBehavior();
            Fire();
        }
    }

    /* non funge
    void OnBecameInvisible()
    {
        if (gameObject != null && gameObject.transform.position.z < (mainCamera.transform.position.z - 100))
            Destroy(gameObject);
    }
    */

    private void FixedUpdate()
    {
        if (!GameState.GetInstance().IsPaused)
        {
            Move();
        }

    }

    public void TakeDamage()
    {
        if (isDestroyable)
        {
            life -= damageOnHit;
            if (life <= 0)
            {
                GameState.GetInstance().AudioManager.PlayExplosion();
                ParticleSystem ps = Instantiate(explosion, transform.position, transform.rotation);
                ps.Play();
                Destroy(gameObject);

                //get points for killing enemies
                PlayerStats.GetInstance().ChangeScore(20);
            }
        }

    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bound")
        {
            GetComponent<Collider>().isTrigger = true;
        }
    }

    public virtual void DestroyOnOutOfScreen()
    {
        if (gameObject.transform.position.z < (mainCamera.transform.position.z - 100))
        {
            Destroy(gameObject);
        }
    }

    public virtual void ApplyBehavior()
    {

    }

}
