﻿

using Patterns;
using System.Collections;
using UnityEngine;

public class GunShipObstacle : AbstractObstacle
{
    private IEnemyState currentState;
    private Rigidbody rb;
    private Collider g_collider;
    private bool canFire = true;

    [SerializeField]
    private GameObject projectile;

    public float thrust = 3f;
    public float fireRateSeconds = 5f;
    public GameObject Projectile { get => projectile; set => projectile = value; }

    private void Start()
    {
        transform.LookAt(Player.transform);
        currentState = new FreeRoamingState(transform, 150f);
        rb = GetComponent<Rigidbody>();
        g_collider = GetComponent<Collider>();
    }

    public override void Fire()
    {
        if (canFire)
        {
            StartCoroutine("ExecuteFire");
            canFire = false;
        }

    }

    private IEnumerator ExecuteFire()
    {
        GameObject projectile = Instantiate(Projectile, transform.position, transform.rotation);
        projectile.GetComponent<Bullet>().shooterType = Bullet.SHOOTER_TYPE.ENEMY;
        yield return new WaitForSeconds(fireRateSeconds);
        canFire = true;
    }


    public override void Move()
    { 
        rb.AddRelativeForce(Vector3.forward * thrust, ForceMode.Force);
        if (transform.position.z < (Player.transform.position.z + 5))
        {
            g_collider.isTrigger = true;
        }
    }

    public override void ApplyBehavior()
    {
        switch (currentState.GetNextState())
        {
            case EnemyPossibleNextState.PlayerTargeting:
                GoToState(new PlayerTargetingState(transform, Player.transform, 20));
                break;
            case EnemyPossibleNextState.GoOutOfScreen:
                GoToState(new GoOutOfScreenState(transform, Player.transform));
                break;
            default: break;
        }
        currentState.Update();
        transform.rotation = Quaternion.Lerp(transform.rotation, currentState.GetTargetRotation(), Time.time * Speed);

    }

    public void GoToState(IEnemyState newState)
    {
        currentState.OnExitState();
        currentState = newState;
        currentState.OnEnterState();
    }

    public void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.Equals(Player))
        {
            ParticleSystem ps = Instantiate(explosion, transform.position, transform.rotation);
            GameState.GetInstance().AudioManager.PlayExplosion();
            ps.Play();
            PlayerStats.GetInstance().UpdateLife(-DamageOnHit);
            Destroy(gameObject);
        }
    }

    public override void DestroyOnOutOfScreen()
    {
        if (gameObject.transform.position.z < (Player.transform.position.z - 3))
        {
            Destroy(gameObject);
        }
    }

    private void OnDrawGizmos()
    {
        if (currentState != null)
        {
            Gizmos.color = new Color(0, 1, 0);
            Gizmos.DrawSphere(currentState.GetTarget(), 1);
        }

    }
}
