﻿

using UnityEngine;

public class AsteroidObstacle : AbstractObstacle
{

    public override void Fire() { }

    public override void Move()
    {
        transform.position -= Vector3.forward*Speed;
    }

    public void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.Equals(Player))
        {
            PlayerStats.GetInstance().UpdateLife(-DamageOnHit);
            Destroy(gameObject);
        }
    }
}
