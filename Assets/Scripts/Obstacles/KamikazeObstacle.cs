﻿

using Patterns;
using UnityEngine;

public class KamikazeObstacle : AbstractObstacle
{
    private IEnemyState currentState; 
    private Rigidbody rb;
    private Collider g_collider;

    public float thrust = 3f;

    private void Start()
    {
        transform.LookAt(Player.transform);
        currentState = new FreeRoamingState(transform); 
        rb = GetComponent<Rigidbody>();
        g_collider = GetComponent<Collider>();
    }

    public override void Fire()
    {  
    }

    public override void Move()
    {  
        rb.AddRelativeForce(Vector3.forward * thrust, ForceMode.Force);
        if (transform.position.z < (Player.transform.position.z + 5))
        {
            g_collider.isTrigger = true;
        }
    }

    public override void ApplyBehavior()
    {
        switch (currentState.GetNextState())
        {
            case EnemyPossibleNextState.PlayerTargeting:
                GoToState(new PlayerTargetingState(transform, Player.transform));
                break;
            case EnemyPossibleNextState.GoOutOfScreen:
                GoToState(new GoOutOfScreenState(transform, Player.transform));
                break;
            default: break;
        }
        currentState.Update();
        transform.rotation = Quaternion.Lerp(transform.rotation, currentState.GetTargetRotation(), Time.time * Speed);

    }

    public void GoToState(IEnemyState newState)
    {
        currentState.OnExitState();
        currentState = newState;
        currentState.OnEnterState();
    } 

    public void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.Equals(Player))
        {
            PlayerStats.GetInstance().UpdateLife(-DamageOnHit);
            ParticleSystem ps = Instantiate(explosion, transform.position, transform.rotation);
            ps.Play();
            GameState.GetInstance().AudioManager.PlayExplosion();
            Destroy(gameObject);
        }
    }

    public override void DestroyOnOutOfScreen()
    {
        if (gameObject.transform.position.z < (Player.transform.position.z - 3))
        {
            Destroy(gameObject);
        }
    }
     
    private void OnDrawGizmos()
    {
        if(currentState != null)
        {
            Gizmos.color = new Color(0, 1, 0);
            Gizmos.DrawSphere(currentState.GetTarget(), 1);
        }
        
    }

}
