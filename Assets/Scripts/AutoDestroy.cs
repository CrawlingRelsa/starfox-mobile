﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class AutoDestroy : MonoBehaviour
{
    ParticleSystem ps;
    bool readyToDestroy;

    private void Start()
    {
        ps = GetComponent<ParticleSystem>();
    }
    private void Update()
    {
        if (ps.isPlaying)
        {
            readyToDestroy = true;
        }
        if(readyToDestroy && ps.isStopped)
        {
            Destroy(gameObject);
        }
    }
}
