﻿

using UnityEngine;

public class ShieldPowerup : AbstractPowerup
{   

    public override void Interact(Collider collision)
    {
        if (collision.gameObject.Equals(Player))
        {

            PlayerStats.GetInstance().UpdateShieldStatus(true);
            Destroy(gameObject); 
        }
    } 
}
