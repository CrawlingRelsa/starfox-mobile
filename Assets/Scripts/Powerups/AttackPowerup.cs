﻿

using UnityEngine;

public class AttackPowerup : AbstractPowerup
{  
    [SerializeField]
    private int attackIncrease = 2;

    public override void Interact(Collider collision)
    {
        if (collision.gameObject.Equals(Player))
        {

            PlayerStats.GetInstance().UpdateAttack(attackIncrease);
            Destroy(gameObject); 
        }
    } 
}
