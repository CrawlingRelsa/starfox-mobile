﻿

using UnityEngine;

public abstract class AbstractPowerup : MonoBehaviour
{ 

    [SerializeField]
    private float speed = 1.0f; 

    [SerializeField]
    private Vector3 direction;

    [SerializeField]
    private bool isDestroyable = true;
     
    private GameObject player;
     
    public float Speed { get => speed; set => speed = value; } 
    public Vector3 Direction { get => direction; set => direction = value; }
    public bool IsDestroyable { get => isDestroyable; set => isDestroyable = value; }
    public GameObject Player { get => player; set => player = value; }
     
    public abstract void Interact(Collider collision);

    private void Awake()
    {
        player = GameObject.FindWithTag("Player");
    }

    private void Start()
    {
        transform.LookAt(player.transform);
    }

    private void Update()
    {
        DestroyOnOutOfScreen();
    }

    private void FixedUpdate()
    {
        if (!GameState.GetInstance().IsPaused)
        {
            Move();
        }            
    }

    public void Move()
    {
        transform.position = Vector3.MoveTowards(transform.position, Player.transform.position, Speed);
    }

    void OnTriggerEnter(Collider collision)
    {
        Interact(collision);
    }
    
    public void DestroyOnOutOfScreen()
    {
        if(gameObject.transform.position.z < Player.transform.position.z)
        { 
            Destroy(gameObject);
        }
    }


}
