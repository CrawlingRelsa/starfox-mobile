﻿

using UnityEngine;

public class LifePowerup : AbstractPowerup
{  
    [SerializeField]
    private int lifeIncrease = 25;

    public override void Interact(Collider collision)
    {
        if (collision.gameObject.Equals(Player))
        {

            PlayerStats.GetInstance().UpdateLife(lifeIncrease);
            Destroy(gameObject); 
        }
    } 
}
