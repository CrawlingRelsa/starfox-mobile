﻿using Patterns;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : Observable
{
    [SerializeField]
    private int currentScore = 0;

    [Header("Basic Stats")]
    public int maxHealth = 100;
    [SerializeField]
    private int life = 100;
    [SerializeField]
    private bool shieldStatus = false; 

    [SerializeField]
    private int bulletPower = 10;

    [SerializeField]
    private GameObject Shield = null;

    public int Life { get => life; set => life = value; }
    public bool ShieldStatus { get => shieldStatus; set => shieldStatus = value; }
    public int BulletPower { get => bulletPower; set => bulletPower = value; }
    public int CurrentScore { get => currentScore; set => currentScore = value; }

    public bool immunity;

    private static PlayerStats instance = null; 

    public static PlayerStats GetInstance()
    { 
        return instance;
    }

    void Start()
    {
        instance = this;
        StartCoroutine("ChangeScoreOnTimePassed");
    }

    // Update is called once per frame
    void Update()
    { 
    }

    public void ChangeScore(int score)
    {
        if (!GameState.GetInstance().IsPaused)
        {
            CurrentScore += score;
            Notify();
        }        
    }

    public IEnumerator ChangeScoreOnTimePassed()
    {
        while (true)
        { 
            ChangeScore(1);
            yield return new WaitForSeconds(1.0f);
        }
    }

    public void UpdateLife(int change)
    {
        //if immunity do not take damage
        if (immunity && change < 0) return;
        //
        bool shouldNotify = true;
        if(change < 0 && Shield.activeSelf)
        {
            UpdateShieldStatus(false);
            change = 0;
            shouldNotify = false;
        }
        Life += change; 
        Life = (Life > maxHealth) ? maxHealth : Life;
        Life = (Life > 0) ? Life : 0;
        if (shouldNotify)
        {
            Notify();
        }  
        if(Life <= 0)
        {
            GameState.GetInstance().GameOver();
        }
    }

    public void UpdateAttack(int change)
    {
        bulletPower += change; 
    }

    public void UpdateShieldStatus(bool status)
    {
        ShieldStatus = status;  
        Shield.SetActive(ShieldStatus);   
    }
     
}
