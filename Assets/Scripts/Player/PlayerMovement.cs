﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement : MonoBehaviour
{
    [Header("Physics")]
    public float acceleration; //accelerazione movimento
    public float torque; //accelerazione angolare
    public float maxAngleTorque = 20; //indica di quanto deve inclinarsi quando vira
    [Header("Parts")]
    public GameObject meshBody; // corpo mesh
    public GameObject sight;
    [Header("Other")]
    public float sightDistance;
    [HideInInspector]
    public Vector3 forwardDirection;

    //private
    private Rigidbody myRigidbody;
    private Vector2 inputAxes;

    public void UpdateInputAxes(Vector2 axes)
    {
        inputAxes = axes;
    }

    // Start is called before the first frame update
    void Start()
    {
        myRigidbody = GetComponent<Rigidbody>();
        transform.position = Camera.main.GetComponent<BoundBehaviour>().GetColliderBounds().position;
    }

    private void Update()
    {
        Quaternion lerpAngle = Quaternion.Lerp(meshBody.transform.rotation, Quaternion.Euler(maxAngleTorque * -inputAxes.y, maxAngleTorque * inputAxes.x, maxAngleTorque * -inputAxes.x), torque * Time.fixedDeltaTime);
        meshBody.transform.rotation = lerpAngle;
        forwardDirection = meshBody.transform.forward;
        sight.transform.position = transform.position + forwardDirection * sightDistance;
    }

    void FixedUpdate()
    {
        if (!GameState.GetInstance().IsPaused)
        {
            myRigidbody.AddForce((Camera.main.transform.right * inputAxes.x + Camera.main.transform.up * inputAxes.y).normalized * acceleration * Time.fixedDeltaTime);
        }
            
    }

    private void OnDrawGizmos()
    {
        //forward axis
        Gizmos.color = Color.green;
        Gizmos.DrawLine(transform.position, transform.position + transform.forward * 2);
    }
}
