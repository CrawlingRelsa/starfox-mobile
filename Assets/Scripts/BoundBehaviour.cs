﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundBehaviour : MonoBehaviour
{
    public GameObject bound; //indica il transform del punto di riferimento
    public float zDepth; //indica la distanza dalla camera
    [Min(0)]
    public float BoundThreshold = 0; //valore con cui diminuire il max Bound
    public float thickness = 1; //spessore dei bound

    private float scaleX;
    private float scaleY;

    void Awake()
    {
        CalculateBounds();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public Transform GetColliderBounds()
    {
        return bound.transform;
    }

    private void CalculateBounds()
    {
        bound.transform.position = transform.position + transform.forward * zDepth;
        Vector3 bottomLeftCorner = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, zDepth));
        Vector3 upLeftCorner = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height, zDepth));
        Vector3 bottomRightCorner = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, zDepth));
        scaleX = (Vector3.Distance(bottomLeftCorner, bottomRightCorner) / 2) - BoundThreshold;
        scaleY = (Vector3.Distance(bottomLeftCorner, upLeftCorner) / 2) - BoundThreshold;
        bound.transform.localScale = new Vector3(scaleX, scaleY, thickness);
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireMesh(bound.GetComponent<MeshCollider>().sharedMesh, bound.transform.position, bound.transform.rotation, new Vector3(scaleX, scaleY, thickness));
    }
}
