﻿using UnityEngine;
using System.Collections;
using Patterns;
using System;

public class Roll : Command
{
    public StateManager stateManager;
    public PlayerMovement playerMovement;
    public Animator playerAnimator;
    public float accelerationBoost;

    
    IState rollState;

    private bool isRolling;

    private void Start()
    {
        rollState = new RollState(accelerationBoost, playerAnimator, playerMovement);
    }


    public override IEnumerator Execute()
    {

        if (!isRolling)
        {
            isRolling = true;
            stateManager.GoToState(rollState);
            yield return new WaitForSeconds(playerAnimator.GetCurrentAnimatorStateInfo(0).length);
            isRolling = false;
            stateManager.GoToState(IdleState.GetInstance());
        }
        else yield return null;
    }

}
