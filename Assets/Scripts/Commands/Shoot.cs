﻿using Patterns;
using System.Collections;
using UnityEngine;

public class Shoot : Command
{
    [Header("Bullet")]
    public GameObject bulletPrefab;
    public float speedShootingInSeconds;
    [Header("Sight")]
    public GameObject sight; //mirino mesh

    //player
    private GameObject player;

    void Start()
    {
        player = FindObjectOfType<PlayerMovement>().gameObject;
    }

    public override IEnumerator Execute()
    {
        GameObject bullet = Instantiate(bulletPrefab, player.transform.position, player.transform.rotation);
        //audio laser
        GameState.GetInstance().AudioManager.PlayLaser();
        bullet.transform.LookAt(sight.transform.position);
        bullet.GetComponent<Bullet>().shooterType = Bullet.SHOOTER_TYPE.PLAYER;
        yield return new WaitForSeconds(speedShootingInSeconds);
        yield return Execute();
    }
}
