﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

//handle single click command (similar to onclick or onkeydown)
public class SingleTapButton : InputButton
{
    public override void OnPointerUp(PointerEventData eventData)
    {
        isClicked = false;
    }
}
