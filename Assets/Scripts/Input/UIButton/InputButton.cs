﻿using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Patterns;

public class InputButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    //click status
    protected bool isClicked;
    //current execution
    protected Coroutine currentExecution;
    //command
    public Command command;

    //TODO: handle graphics feedback

    public virtual void OnPointerDown(PointerEventData eventData)
    {
        if (command != null && !GameState.GetInstance().IsPaused)
        {
            isClicked = true;
            currentExecution = StartCoroutine(command.Execute());
        }
    }

    public virtual void OnPointerUp(PointerEventData eventData)
    {
        if (command != null && isClicked)
        {
            isClicked = false;
            StopCoroutine(currentExecution);
        }
    }

}
