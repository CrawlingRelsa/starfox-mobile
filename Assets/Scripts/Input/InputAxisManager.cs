﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Patterns;

//input controller
public class InputAxisManager : MonoBehaviour, IObserver
{
    public IInputPad inputClass; //classe da guardare per gli input
    public PlayerMovement player; //giocatore

    private Vector2 coordinates;

    void Start()
    {
        AttachTo(inputClass);
    }

    public void AttachTo(IInputPad observable)
    {
        observable.AddObserver(this);
    }

    public void GetUpdateFrom(Observable observable)
    {
        if (observable is IInputPad pad)
        {
            Vector2 coordinates = pad.GetAxesCoordinates();
            player.UpdateInputAxes(coordinates);
        }
    }
}
