﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Patterns;

public class IInputPad : Observable
{
    protected Vector2 axes;
    public bool isEnabled;

    public Vector2 GetAxesCoordinates()
    {
        return axes;
    }
}
