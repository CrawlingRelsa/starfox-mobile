﻿using System;
using System.Collections.Generic;
using UnityEngine;


public class KeyboardInput : IInputPad
{
    // Update is called once per frame
    void Update()
    {
        if (isEnabled)
        {
            axes = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            Notify();
        }

    }
}
