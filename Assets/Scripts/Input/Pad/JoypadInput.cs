﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoypadInput : IInputPad
{
    [SerializeField]
    private bl_Joystick Joystick = null;//Joystick reference for assign in inspector

    private Vector2 direction;
    // Update is called once per frame
    void Update()
    {
        if (isEnabled)
        {
            direction.x = Joystick.Horizontal;
            direction.y = Joystick.Vertical;
            direction.Normalize();
            axes = direction; 
            //notify controller
            Notify();
        }
    }

}
