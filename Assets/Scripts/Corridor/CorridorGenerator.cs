﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class CorridorGenerator : MonoBehaviour
{
    [SerializeField]
    private int gridRows = 10;

    [SerializeField]
    private int gridCols = 10; 

    [SerializeField]
    private float cellDistance = 2.0f; 

    [SerializeField]
    private int MAX_OBSTACLES_PER_GRID = 10;

    [SerializeField]
    private int MAX_POWERUPS_PER_GRID = 1;

    [SerializeField]
    private float enemyPresenceProbability = 0.3f;

    [SerializeField]
    private float powerupPresenceProbability = 0.3f;

    [SerializeField]
    private GameObject[] enemyTypes = new GameObject[3];

    [SerializeField]
    private GameObject[] powerupTypes = new GameObject[3]; 

    [SerializeField]
    private float timeBetweenGrids = 5f;

    private Vector3 startingPoint = Vector3.zero;
     
    public Vector3 Offset;
    public int GridRows { get => gridRows; set => gridRows = value; }
    public int GridCols { get => gridCols; set => gridCols = value; } 
    public float CellDistance { get => cellDistance; set => cellDistance = value; }
    public GameObject[] EnemyTypes { get => enemyTypes; set => enemyTypes = value; }
    public GameObject[] PowerupTypes { get => powerupTypes; set => powerupTypes = value; } 

    public static bool ShouldCreate = false;


    void Awake()
    {
        startingPoint += Offset; 
    }

    private void Start()
    {
        StartCoroutine("GridGenerator");
    }  

    IEnumerator GridGenerator()
    {
        while (!GameState.GetInstance().IsGameOver)
        {
            if (!GameState.GetInstance().IsPaused)
            {
                yield return new WaitForSeconds(timeBetweenGrids);
                GenerateGrid();
            }
            else
            {
                yield return new WaitUntil(() => !GameState.GetInstance().IsPaused);
            }

        }
        
    }

    void GenerateGrid()
    {
        int maxObstaclesPerGrid = MAX_OBSTACLES_PER_GRID;
        int maxPowerupsPerGrid = MAX_POWERUPS_PER_GRID; 

        for (int i=0; i < GridCols; i++)
        {
            for (int j = 0; j < GridRows; j++)
            { 
                int objIndex = GetRandomGameObject(enemyPresenceProbability, enemyTypes, maxObstaclesPerGrid);
                if (objIndex >= 0)
                {
                    maxObstaclesPerGrid--;
                    GameObject generated = Instantiate(enemyTypes[objIndex]);
                    generated.transform.position = startingPoint + new Vector3(CellDistance * j, CellDistance * i, 0);  
                }
                else
                {
                    objIndex = GetRandomGameObject(powerupPresenceProbability, powerupTypes, maxPowerupsPerGrid);
                    if (objIndex >= 0)
                    {
                        maxPowerupsPerGrid--;
                        GameObject generated = Instantiate(powerupTypes[objIndex]);
                        generated.transform.position = startingPoint + new Vector3(CellDistance * j, CellDistance * i, 0); 
                    }
                }  

            }
        }
    }  

    private int GetRandomGameObject(float elementPresenceProbability, GameObject[] elementList, int maxElementsPerGrid)
    {
        if (maxElementsPerGrid > 0)
        {

            float isThereElemProb = Random.Range(0f, 1f);
            if (isThereElemProb <= elementPresenceProbability)
            {

                return Random.Range(-1, elementList.Length);
            }
            else
            {
                return -1; 
            }
        } else
        {
            return -1; 
        }
        
    }
}