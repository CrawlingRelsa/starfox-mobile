﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Bullet : MonoBehaviour
{
    public float speed = 20f;
    public float lifeTime;
    public int damage;

    public SHOOTER_TYPE shooterType;

    // Start is called before the first frame update
    void Start()
    {
        Rigidbody myRigidbody = GetComponent<Rigidbody>();
        myRigidbody.AddForce(transform.forward * speed, ForceMode.Impulse);
        StartCoroutine(Timer());
    }

    private void OnTriggerEnter(Collider collision)
    {
        GameObject collObject = collision.gameObject;
        if (collObject.tag == "Player" && shooterType == SHOOTER_TYPE.ENEMY)
        {
            PlayerStats.GetInstance().UpdateLife(-damage);
        }
        if (collObject.tag == "Enemy" && shooterType == SHOOTER_TYPE.PLAYER)
        {
            collision.GetComponent<AbstractObstacle>().TakeDamage();
        }
    }

    IEnumerator Timer()
    {
        yield return new WaitForSeconds(lifeTime);
        Destroy(gameObject);
    }

    public enum SHOOTER_TYPE
    {
        PLAYER, ENEMY
    }
}
