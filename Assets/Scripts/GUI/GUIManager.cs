﻿using System.Collections;
using System.Collections.Generic;
using Patterns;
using UnityEngine;
using UnityEngine.UI;

public class GUIManager : MonoBehaviour, IObserver
{
    public Observable PlayerStats;
    private Slider healthSlider;
    private Text scoreText;

    public GameObject HealthSlider;
    public GameObject ScoreText; 

    // Start is called before the first frame update
    void Start()
    {
        healthSlider = HealthSlider.GetComponent<Slider>();
        scoreText = ScoreText.GetComponent<Text>();
        PlayerStats.AddObserver(this);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void GetUpdateFrom(Observable observable)
    {
        PlayerStats ps = (PlayerStats)observable;

        healthSlider.value = ps.Life;
        scoreText.text = "Score: " + ps.CurrentScore; 
    }
}
