﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseController : MonoBehaviour
{
    public void GoToScene(string gameScene)
    {
        SceneManager.LoadScene(gameScene);
    }

    public void TogglePauseMenu(bool status)
    {
        if (status)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
        GameState.GetInstance().IsPaused = status;
        gameObject.SetActive(status);
    }
}
