﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{

    [SerializeField]
    private GameObject scoreText = null; 
    [SerializeField]
    private GameObject highscoreText = null;

    public GameObject creditsPanel;


    public void GoToScene(string gameScene)
    {
        SceneManager.LoadScene(gameScene);
    } 

    public void CloseGame()
    {
        Application.Quit();
    }

    private void Start()
    {
        showScore();
    }

    public void ShowCredits(bool isActive)
    {
        creditsPanel.SetActive(isActive);
    }

    private void showScore()
    {
        int last_score = PlayerPrefs.GetInt("last_score");
        int high_score = PlayerPrefs.GetInt("high_score");
        bool new_high = (PlayerPrefs.GetInt("new_high") == 1) ? true : false; 
        PlayerPrefs.DeleteKey("new_high");
        if (new_high)
        { 
            highscoreText.GetComponent<Text>().text = "New high score: " + high_score + "!";   
        }
        else
        {
            if(scoreText != null)
            {
                scoreText.GetComponent<Text>().text = "Score: " + last_score;
            }            
            highscoreText.GetComponent<Text>().text = "High score: " + high_score;
        }
        
    }

}
