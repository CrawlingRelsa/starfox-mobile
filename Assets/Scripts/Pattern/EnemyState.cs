﻿using UnityEngine;
using System.Collections;

namespace Patterns
{
    public interface IEnemyState
    { 
        void Update();
        void OnEnterState();
        void OnExitState();
        Quaternion GetTargetRotation();
        Vector3 GetTarget();
        EnemyPossibleNextState GetNextState();
    }

    public enum EnemyPossibleNextState
    {
        Current,
        FreeRoaming,
        PlayerTargeting,
        GoOutOfScreen
    }
}
