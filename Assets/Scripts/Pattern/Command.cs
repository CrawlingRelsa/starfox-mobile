﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Patterns
{
    public abstract class Command : MonoBehaviour
    {
        public abstract IEnumerator Execute();
    }
}

