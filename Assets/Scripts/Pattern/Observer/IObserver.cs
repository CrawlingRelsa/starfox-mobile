﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Patterns
{
    public interface IObserver
    {
        void GetUpdateFrom(Observable observable);
    }

}
