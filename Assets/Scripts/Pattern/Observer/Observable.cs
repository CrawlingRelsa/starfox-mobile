﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Patterns      
{
    public class Observable : MonoBehaviour
    {
        List<IObserver> observers = new List<IObserver>();

        public void AddObserver(IObserver observer)
        {
            observers.Add(observer);
        }

        public void RemoveObserver(IObserver observer)
        {
            observers.Remove(observer);
        }

        public void Notify()
        {
            foreach (var observer in observers)
            {
                observer.GetUpdateFrom(this);
            }
        }

    }
}

