﻿using UnityEngine;
using System.Collections;

namespace Patterns
{
    public interface IState
    {
        void Update();
        void OnEnterState();
        void OnExitState();

    }
}
