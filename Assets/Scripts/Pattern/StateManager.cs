﻿using UnityEngine;
using System.Collections;
using Patterns;

public class StateManager: MonoBehaviour
{
    IState currentState;


    private void Start()
    {
        currentState = IdleState.GetInstance();
    }

    public void GoToState(IState newState)
    {
        currentState.OnExitState();
        currentState = newState;
        currentState.OnEnterState();
    }

    private void Update()
    {
        currentState.Update();
    }
}
