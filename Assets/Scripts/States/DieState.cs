﻿using UnityEngine;
using System.Collections;
using Patterns;

public class DieState : IState
{
    private GameObject target;
    private ParticleSystem particleSystem;

    public DieState(GameObject target, ParticleSystem particleSystem)
    {
        this.target = target;
        this.particleSystem = particleSystem;
    }

    public void OnEnterState()
    {
        target.transform.localScale = Vector3.zero;
        particleSystem.Play();
        GameState.GetInstance().AudioManager.PlayExplosion();

    }

    public void OnExitState() { }

    public void Update() { }
}
