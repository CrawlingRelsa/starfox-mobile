﻿using Patterns;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeRoamingState : IEnemyState
{
    private Vector3 target;
    private Transform enemyTransform;
    private Quaternion toRotation;
    private EnemyPossibleNextState nextState;
    private float offset;

    public FreeRoamingState(Transform transform, float offset = 100)
    {
        enemyTransform = transform;
        nextState = EnemyPossibleNextState.Current;
        this.offset = offset;
        OnEnterState();
    }

    public void Update()
    {
        if(enemyTransform.position.z < target.z)
        {
            nextState = EnemyPossibleNextState.PlayerTargeting;
        }    
    }

    public void OnEnterState()
    {
        target = new Vector3(Random.Range(-40f, 40f), Random.Range(-40, 40f), Random.Range(offset-10f, offset+10f));
        Vector3 direction = target - enemyTransform.position;
        toRotation = Quaternion.LookRotation(direction);
    }

    public void OnExitState()
    { 
    } 
 
    public Quaternion GetTargetRotation()
    {
        return toRotation;
    } 

    public EnemyPossibleNextState GetNextState()
    {
        return nextState;
    }

    public Vector3 GetTarget()
    {
        return target;
    }
}
