﻿using Patterns;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTargetingState : IEnemyState
{

    private Vector3 target;
    private Transform transform;
    private Transform targetTransform;
    private Quaternion toRotation;
    private EnemyPossibleNextState nextState;
    private float offset;

    public PlayerTargetingState(Transform enemyTransform, Transform targetTransform, float offset = 5)
    {
        transform = enemyTransform;
        this.targetTransform = targetTransform;
        nextState = EnemyPossibleNextState.Current;
        target = targetTransform.position; 
        this.offset = offset;
    }

    public Quaternion GetTargetRotation()
    {
        return toRotation;
    } 

    public void OnEnterState()
    { 
    }

    public void OnExitState()
    { 
    }  

    public void Update()
    {
        Vector3 direction = targetTransform.position - transform.position;
        toRotation = Quaternion.LookRotation(direction);
        if (transform.position.z < (targetTransform.position.z + offset))
        {
            nextState = EnemyPossibleNextState.GoOutOfScreen;
        }

    }

    public EnemyPossibleNextState GetNextState()
    {
        return nextState;
    }

    public Vector3 GetTarget()
    {
        return target;
    }
}
