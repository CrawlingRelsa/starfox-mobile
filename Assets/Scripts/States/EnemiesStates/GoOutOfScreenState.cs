﻿using Patterns;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoOutOfScreenState : IEnemyState
{

    private Vector3 target;
    private Transform transform;
    private Transform targetTransform;
    private Quaternion toRotation;
    private EnemyPossibleNextState nextState;
    private float offset;

    public GoOutOfScreenState(Transform enemyTransform, Transform targetTransform)
    {
        transform = enemyTransform;
        this.targetTransform = targetTransform;
        nextState = EnemyPossibleNextState.Current;
        target = targetTransform.position;
        offset = 2f;
    }

    public Quaternion GetTargetRotation()
    {
        return toRotation;
    } 

    public void OnEnterState()
    { 
    }

    public void OnExitState()
    { 
    }  

    public void Update()
    {
        Vector3 direction = (targetTransform.position - targetTransform.forward*offset) - transform.position;
        toRotation = Quaternion.LookRotation(direction);
        offset += 0.1f;

    }

    public EnemyPossibleNextState GetNextState()
    {
        return nextState;
    }

    public Vector3 GetTarget()
    {
        return target;
    }
}
