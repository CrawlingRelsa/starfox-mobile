﻿using UnityEngine;
using System.Collections;
using Patterns;

public class IdleState : IState
{
    private IdleState() { }

    #region SINGLETON
    private static IdleState singleton;
    public static IdleState GetInstance()
    {
        if (singleton == null)
            singleton = new IdleState();
        return singleton;
    }
    #endregion

    public void Update(){}

    public void OnEnterState(){}

    public void OnExitState(){}
}
