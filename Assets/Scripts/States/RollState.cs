﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using Patterns;

public class RollState : IState
{
    float accelerationBoost;
    Animator playerAnimator;
    PlayerMovement playerMovement;

    public RollState(float accelerationBoost, Animator playerAnimator, PlayerMovement playerMovement)
    {
        this.accelerationBoost = accelerationBoost;
        this.playerAnimator = playerAnimator;
        this.playerMovement = playerMovement;
    }

    public void Update(){}

    public void OnEnterState()
    {
        playerAnimator.SetBool("isRolling", true);
        playerMovement.acceleration += accelerationBoost;
        PlayerStats.GetInstance().immunity = true;
        GameState.GetInstance().AudioManager.PlayRoll();
    }

    public void OnExitState()
    {
        playerMovement.acceleration -= accelerationBoost;
        playerAnimator.SetBool("isRolling", false);
        PlayerStats.GetInstance().immunity = false;
    }
}
