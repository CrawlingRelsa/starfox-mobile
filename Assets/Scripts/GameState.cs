﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;

public class GameState : MonoBehaviour
{
    private static GameState instance = null;

    public AudioManager AudioManager;

    public string gameOverSceneName = "GameOver";
    private Camera mainCamera;

    public StateManager playerStateManager;
    [SerializeField]
    private GameObject startMessage = null;
    [SerializeField]
    private GameObject interactable = null;

    public bool IsPaused { get; set; }
    public bool IsGameOver { get; private set; }


    public static GameState GetInstance()
    {
        return instance;
    }

    private void Awake()
    {

        instance = this;
        mainCamera = Camera.main;
        mainCamera.fieldOfView = 175;
    }

    void Start()
    {
        StartCoroutine("UpdateView");
    }

    private void Update()
    {
    }

    IEnumerator UpdateView()
    {
        float transitionTime = 0.02f;
        while (mainCamera.fieldOfView > 90)
        {
            mainCamera.fieldOfView--;
            yield return new WaitForSeconds(transitionTime);
        }
        startMessage.SetActive(true);
        while (mainCamera.fieldOfView > 55)
        {
            mainCamera.fieldOfView--;
            yield return new WaitForSeconds(transitionTime);
            transitionTime += 0.001f;
        }
        startMessage.SetActive(false); 
        interactable.SetActive(true);

    }

    public void GameOver()
    {
        IsGameOver = true;
        saveScore();
        StartCoroutine(GameOverAnimation());
    }

    IEnumerator GameOverAnimation()
    {
        playerStateManager.GoToState(
            new DieState(
                playerStateManager.gameObject,
                playerStateManager.GetComponentsInChildren<ParticleSystem>().First(
                    (ParticleSystem ps) =>
                    {
                        return ps.name == "Explosion";
                    }
                )));
        yield return new WaitForSeconds(1.3f);
        SceneManager.LoadScene(gameOverSceneName);
    }


    private void saveScore()
    {
        int last_score = PlayerStats.GetInstance().CurrentScore;
        PlayerPrefs.SetInt("last_score", last_score);
        int high_score = PlayerPrefs.GetInt("high_score");
        if (high_score < last_score)
        {
            PlayerPrefs.SetInt("high_score", last_score);
            PlayerPrefs.SetInt("new_high", 1);
        }
    }
}
