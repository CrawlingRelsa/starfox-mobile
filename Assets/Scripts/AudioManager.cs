﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioSource explosion;
    public AudioSource laser;
    public AudioSource roll;

    public void PlayExplosion()
    {
        explosion.Play();
    }

    public void PlayLaser()
    {
        laser.Play();
    }

    public void PlayRoll()
    {
        roll.Play();
    }
     
}
